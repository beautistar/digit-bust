//
//  HowToViewController.swift
//  Digit Bust
//
//  Created by Beautistar on 9/4/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class HowToViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var tvHowTo: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        label.text = "    You have 90 seconds to enter as many codes correctly before time runs out.\n\n    If mess up code order then press reset and can try again but will lost 2 seconds.\n\n    Press Bust when believe digits are entered correctly. If entered correctly a new code will appear and digits will scramable. If entered incorrectly then will lost 5 seconds and new code will appear.\n\n\n                  Good Luck"
        let strHowTo = (label.text)!
        let underlineAttriString = NSMutableAttributedString(string: strHowTo)
        let range1 = (strHowTo as NSString).range(of: "reset")
        underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        let range2 = (strHowTo as NSString).range(of: "Bust")
        underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        label.attributedText = underlineAttriString
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
